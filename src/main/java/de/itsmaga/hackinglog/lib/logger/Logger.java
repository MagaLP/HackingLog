package de.itsmaga.hackinglog.lib.logger;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Erstellt von ItsMaga
 */

public class Logger {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    @Getter
    private final List<String> loggedMessages;

    public Logger(){
        this.loggedMessages = new CopyOnWriteArrayList<>();
    }

    public void log(LoggerState state, String message){
        /**
         * Print out
         */
        System.out.println("[MS] ".concat(state.getPrefix().concat(" ").concat(message)));
        /**
         * Save into list
         */
        Date date = new Date();
        String messageDate = simpleDateFormat.format(date);
        getLoggedMessages().add(messageDate);
    }


    @Getter
    @AllArgsConstructor
    public enum  LoggerState {
        INFO("Info >"),
        WARNING("Warning >"),
        SUCCESS("Success >");

        private final String prefix;
    }
}


