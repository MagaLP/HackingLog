package de.itsmaga.hackinglog.lib.file;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public abstract class AbstractJsonFile {

    private final static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Map<String, Object> keys;
    private File file;


    /**
     * @Path where the file come
     * @File don't forget the file ending .json
     * @param path
     * @param fileName
     */
    public AbstractJsonFile(String path, String fileName) {
        File directory = new File(path);
        directory.mkdirs();
        setFile(new File(path + fileName));
        write();

    }


    /**
     * Write defaults into the file
     */
    private void write(){
        if(!getFile().exists()){
            try {
                getFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            setKeys(new HashMap<>());
            create();
            save();
        } else {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(getFile())));
                setKeys(gson.fromJson(reader, HashMap.class));
            }catch(FileNotFoundException ex){
                ex.printStackTrace();
            }

        }
    }

    /**
     * Put key and object into the cache
     * @param key
     * @param object
     */
    public void set(String key, Object object){
        getKeys().put(key, object);
    }


    /**
     * Get object from the cache
     * @param key
     * @return
     */
    public Object getObject(String key){
        if(!getKeys().containsKey(key)){
            return null;
        }
        return getKeys().get(key);
    }


    /**
     * Save the current cache into the json file
     */
    public void save(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonConfiguration = gson.toJson(getKeys());
        try {
            FileWriter writer = new FileWriter(getFile());
            writer.write(jsonConfiguration);
            writer.flush();
            writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Default
     */
    public abstract void create();

}
