package de.itsmaga.hackinglog.lib.file.config;

import de.itsmaga.hackinglog.lib.file.AbstractJsonFile;

/**
 * Erstellt von ItsMaga
 */
public class ConfigFile extends AbstractJsonFile{

    public final String hostField = "host";
    public final String portField = "port";
    public final String databaseField = "database";
    public final String userField = "user";
    public final String passwordField = "password";
    public final String tableField = "tableName";

    public ConfigFile(String path) {
        super(path, "config.json");
    }

    @Override
    public void create() {
        set(hostField, "localhost");
        set(portField, 3306);
        set(databaseField, "database");
        set(userField, "user");
        set(passwordField, "password");
        set(tableField, "mslog");
    }

    public String getHost(){
        return (String)getObject(hostField);
    }

    public int getPort(){
        double oldValue = (double)getObject(portField);
        return (int)oldValue;
    }

    public String getDatabase(){
        return (String)getObject(databaseField);
    }

    public String getUser(){
        return (String)getObject(userField);
    }

    public String getPassword(){
        return (String)getObject(passwordField);
    }

    public String getTable(){
        return (String)getObject(tableField);
    }
}
