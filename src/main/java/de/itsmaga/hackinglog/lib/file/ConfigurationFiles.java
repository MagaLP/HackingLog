package de.itsmaga.hackinglog.lib.file;

import de.itsmaga.hackinglog.lib.file.config.ConfigFile;
import lombok.Getter;
import lombok.Setter;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public class ConfigurationFiles {

    /**
     * Files
     */
    private ConfigFile configFile;


    /**
     * Initalize Files
     * @param path
     */
    public ConfigurationFiles(String path) {
        setConfigFile(new ConfigFile(path));
    }
}
