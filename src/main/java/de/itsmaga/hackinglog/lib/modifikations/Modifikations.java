package de.itsmaga.hackinglog.lib.modifikations;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Erstellt von ItsMaga
 */
@Getter
@AllArgsConstructor
public enum Modifikations {

    FLY("fly", 5);

    private String column;
    private final int highLevel;


}
