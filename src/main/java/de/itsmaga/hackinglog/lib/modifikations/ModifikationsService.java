package de.itsmaga.hackinglog.lib.modifikations;

import com.google.common.base.Preconditions;

/**
 * Erstellt von ItsMaga
 */
public class ModifikationsService {


    public String getModifikationsLevelColor(final Modifikations modifikations, int level){
        if(level >= modifikations.getHighLevel()){
            return "§4";
        } else if(level >= (modifikations.getHighLevel()/2) && level < modifikations.getHighLevel()) {
            return "§c";
        } else {
            return "§6";
        }
    }

    public Modifikations getModifikationByName(final String name){
        for(Modifikations modifikations : Modifikations.values()){
            if(modifikations.name().equalsIgnoreCase(name)){
                return modifikations;
            }
        }
        return null;
    }

    public boolean checkModifikation(final String name){
        Preconditions.checkNotNull(name);
        for(Modifikations modifikations : Modifikations.values()){
            if(modifikations.name().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }
}
