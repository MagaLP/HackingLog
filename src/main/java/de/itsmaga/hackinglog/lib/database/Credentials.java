package de.itsmaga.hackinglog.lib.database;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Erstellt von ItsMaga
 */
@Getter
@AllArgsConstructor
public class Credentials {

    private String host, user, password, database;
    private int port;


}
