package de.itsmaga.hackinglog.lib.database.callback;

/**
 * Erstellt von ItsMaga
 */
public interface Callback<T> {

    void call(T t);
}
