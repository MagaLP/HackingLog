package de.itsmaga.hackinglog.lib.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Erstellt von ItsMaga
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AsyncDatabase {

    /**
     * AsyncDatabase is to mark connectors that the database is async
     */

}
