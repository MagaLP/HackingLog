package de.itsmaga.hackinglog.lib.database;

import lombok.Getter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Erstellt von ItsMaga
 */
@Getter
public abstract class DatabaseConnector {



    public static final ExecutorService POOL = Executors.newCachedThreadPool();
    private final Credentials credentials;
    private final String primaryKey;
    private final boolean async;

    /**
     * @param credentials
     */
    public DatabaseConnector(Credentials credentials, String primaryKey) {
        this.credentials = credentials;
        this.primaryKey = primaryKey;
        Class<?> clazz = this.getClass();
        this.async = clazz.isAnnotationPresent(AsyncDatabase.class);
    }

    /**
     * Write object into the database
     * @param primaryKey
     * @param value
     */
    public void write(final String primaryKey, final String key, final Object value){
        if(!isAsync()){
            defaultWrite(primaryKey, key, value);
        } else {
            POOL.execute(() -> {
                defaultWrite(primaryKey, key, value);
            });
        }
    }

    /**
     * It is sync because I'm using it for bungeecord and bungeecord is already async
     * so you don't need to make it async
     * Read object from the database
     * @param primaryKey
     * @return
     */
    public Object read(String primaryKey, String key){
        return defaultRead(primaryKey, key);
    }

    /**
     * Check if the primarykey exists in the database
     * @param primaryKey
     * @return
     */
    public boolean checkPrimary(final String primaryKey){
        return defaultCheckPrimary(primaryKey);
    }




    /**
     * Custom write for universal database
     * @param primaryKey
     * @param value
     */
    public abstract void defaultWrite(String primaryKey, String key, Object value);
    public abstract Object defaultRead(String primaryKey, String key);
    public abstract boolean defaultCheckPrimary(String primaryKey);

    /**
     * Connect & disconnect
     */
    public abstract void connect() throws Exception;
    public abstract void disconnect() throws Exception;
    public abstract boolean isOpen() throws Exception;
}
