package de.itsmaga.hackinglog.spigot.setup;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Erstellt von ItsMaga
 */
@Getter
public class Setup {

    public final String fallbackPrefix = "hackinglog";

    @Setter
    private final List<Command> commands;
    private final List<Listener> listeners;
    private final Plugin plugin;

    /**
     * @param plugin
     */
    public Setup(Plugin plugin) {
        this.commands = new ArrayList<>();
        this.listeners = new ArrayList<>();
        this.plugin = plugin;
    }

    /**
     * Add command to list
     * @param command
     */
    public void addCommand(Command command){
        if(!commands.contains(command)){
            this.commands.add(command);
        }
    }

    /**
     * Add listener to list
     * @param listener
     */
    public void addListener(Listener listener){
        if(!listeners.contains(listener)){
            getListeners().add(listener);
        }
    }


    /**
     * Register commands & listeners
     * without putting commands in plugin.yml
     */
    public void register(){
        CraftServer server = (CraftServer) Bukkit.getServer();
        commands.forEach(new Consumer<Command>() {
            @Override
            public void accept(Command command) {
                server.getCommandMap().register(fallbackPrefix, command);
            }
        });
        getListeners().forEach(new Consumer<Listener>() {
            @Override
            public void accept(Listener listener) {
                getPlugin().getServer().getPluginManager().registerEvents(listener, getPlugin());
            }
        });
    }
}
