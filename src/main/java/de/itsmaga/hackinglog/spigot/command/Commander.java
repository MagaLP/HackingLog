package de.itsmaga.hackinglog.spigot.command;

import de.itsmaga.hackinglog.spigot.command.anno.Permission;
import de.itsmaga.hackinglog.spigot.command.anno.Senders;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

/**
 * Erstellt von ItsMaga
 */
public abstract class Commander extends Command{


    /**
     * Constructor
     * @Name is the command name
     * @Aliases are the subnames from the command
     * @param name
     * @param aliases
     */
    public Commander(String name, String... aliases) {
        super(name);
        setAliases(Arrays.asList(aliases));
    }

    /**
     * Default spigot command execute
     * @param sender
     * @param label
     * @param args
     * @return
     */
    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        if(!this.getClass().isAnnotationPresent(Senders.class)){
            return true;
        }
        Class<?> clazz = this.getClass();
        Senders senders = clazz.getAnnotation(Senders.class);
        if(!isSender(senders.senders(), sender.getClass())){
            return true;
        }
        if(!this.getClass().isAnnotationPresent(Permission.class)){
            send(sender, args);
            return true;
        }
        Permission permission = clazz.getAnnotation(Permission.class);
        if(!sender.hasPermission(permission.permission())){
            sender.sendMessage(permission.message());
            return true;
        }
        send(sender, args);
        return true;
    }

    /**
     * Check if the class is in the array
     * @param array
     * @param sender
     * @return
     */
    private boolean isSender(Class<?>[] array, Class sender){
        for(Class<?> arrayClass : array){
            if(arrayClass == sender){
                return true;
            }
        }
        return false;
    }

    /**
     * Custom executer command
     * @param sender
     * @param args
     * @param <T>
     */
    public abstract <T> void send(T sender, String[] args);
}