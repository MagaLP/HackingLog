package de.itsmaga.hackinglog.spigot.command.impl;

import de.itsmaga.hackinglog.lib.logger.Logger;
import de.itsmaga.hackinglog.lib.modifikations.Modifikations;
import de.itsmaga.hackinglog.lib.modifikations.ModifikationsService;
import de.itsmaga.hackinglog.spigot.HackingLogSpigot;
import de.itsmaga.hackinglog.spigot.command.Commander;
import de.itsmaga.hackinglog.spigot.command.anno.Senders;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
@Senders(senders = ConsoleCommandSender.class)
public class HackingCommand extends Commander{

    public HackingCommand() {
        super("hacking");
    }

    @Override
    public <T> void send(T sender, String[] args) {
        ConsoleCommandSender consoleSender = (ConsoleCommandSender)sender;
        ModifikationsService service = HackingLogSpigot.getInstance().getModifikationsService();

        String modifikationsName = args[0];
        if(!service.checkModifikation(modifikationsName)){
            return;
        }
        Modifikations modifikations = service.getModifikationByName(modifikationsName);

        String playerName = args[1];
        if(Bukkit.getPlayer(playerName) == null){
            return;
        }
        Player player = Bukkit.getPlayer(playerName);
        UUID uuid = player.getUniqueId();
        HackingLogSpigot.getInstance().getLogging().log(Logger.LoggerState.INFO, "Player ".concat(player.getName()).concat(" becomes suspicious for modifications."));
        /**
         * Add value to database
         */
        HackingLogSpigot.getInstance().getMySQLConnector().addModifkationLevel(uuid, modifikations, 1);

    }
}
