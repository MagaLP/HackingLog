package de.itsmaga.hackinglog.spigot.command;

import de.itsmaga.hackinglog.spigot.command.impl.HackingCommand;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Erstellt von ItsMaga
 */
@Getter
public class CommandRegistry {


    public final List<Commander> commands;

    /**
     * Constructor
     * register default commands
     */
    public CommandRegistry() {
        this.commands = new ArrayList<>();
        register();
    }

    /**
     * Adding commands to list
     */
    private void register(){
        getCommands().add(new HackingCommand());
    }


}
