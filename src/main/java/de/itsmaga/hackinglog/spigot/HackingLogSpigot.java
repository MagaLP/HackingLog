package de.itsmaga.hackinglog.spigot;

import de.itsmaga.hackinglog.lib.database.Credentials;
import de.itsmaga.hackinglog.lib.file.ConfigurationFiles;
import de.itsmaga.hackinglog.lib.logger.Logger;
import de.itsmaga.hackinglog.lib.modifikations.ModifikationsService;
import de.itsmaga.hackinglog.spigot.command.CommandRegistry;
import de.itsmaga.hackinglog.spigot.mysql.MySQLConnector;
import de.itsmaga.hackinglog.spigot.setup.Setup;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Erstellt von ItsMaga
 */
@Getter
public class HackingLogSpigot extends JavaPlugin{

    @Getter @Setter
    private static HackingLogSpigot instance;

    private CommandRegistry commandRegistry;
    private ModifikationsService modifikationsService;
    private ConfigurationFiles configurationFiles;
    private MySQLConnector mySQLConnector;
    private Logger logging;


    @Override
    public void onLoad() {
        setInstance(this);
    }

    @Override
    public void onEnable() {
        initalize();
    }


    @Override
    public void onDisable() {


    }

    private void initalize(){

        /**
         * Logger
         */
        Logger logger = new Logger();
        this.logging = logger;

        /**
         * CommandRegistry
         */
        CommandRegistry commandRegistry = new CommandRegistry();
        this.commandRegistry = commandRegistry;

        /**
         * ModifikationsService
         */
        ModifikationsService modifikationsService = new ModifikationsService();
        this.modifikationsService = modifikationsService;

        /**
         * ConfigurationFiles
         */
        ConfigurationFiles configurationFiles = new ConfigurationFiles(getDataFolder().getPath());
        this.configurationFiles = configurationFiles;

        /**
         * MySQLConnector
         */
        Credentials credentials = new Credentials(configurationFiles.getConfigFile().getHost(), configurationFiles.getConfigFile().getUser(),
                configurationFiles.getConfigFile().getPassword(), configurationFiles.getConfigFile().getDatabase(), configurationFiles.getConfigFile().getPort());
        MySQLConnector mySQLConnector = new MySQLConnector(credentials, configurationFiles.getConfigFile().getTable());
        try {
            mySQLConnector.connect();
        } catch (Exception e) {
            e.printStackTrace();
            getLogging().log(Logger.LoggerState.WARNING, "Could not connect to database");
        }
        this.mySQLConnector = mySQLConnector;
        /**
         * Setup
         */
        registerSetup(commandRegistry);
    }

    /**
     * Register commands & listeners
     * @param registry
     */
    private void registerSetup(CommandRegistry registry){
        Setup setup = new Setup(this);
        /**
         * Commands
         */
        registry.getCommands().forEach((commander) -> {
            setup.addCommand(commander);
        });

        /**
         * Listeners
         */

        //--------------------
        setup.register();
    }
}
