package de.itsmaga.hackinglog.spigot.mysql;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import de.itsmaga.hackinglog.lib.database.AsyncDatabase;
import de.itsmaga.hackinglog.lib.database.Credentials;
import de.itsmaga.hackinglog.lib.database.DatabaseConnector;
import de.itsmaga.hackinglog.lib.modifikations.Modifikations;
import lombok.Getter;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
@AsyncDatabase
public class MySQLConnector extends DatabaseConnector{

    public final String driver = "com.mysql.jdbc.Driver";
    @Getter
    private Connection connection;
    @Getter
    private final String tableName;
    public final String updateQuery = "UPDATE %s SET ? = ? WHERE ? = ?";
    public final String resultQuery = "SELECT * FROM %s WHERE %s = %s";



    public MySQLConnector(Credentials credentials, String tableName) {
        super(credentials, "uuid");
        this.tableName = tableName;
    }


    public void addModifkationLevel(UUID uuid, Modifikations modifikations, int level){
        if(!checkPrimary(uuid.toString())){
            return;
        }
        int currentLevel = getModifikationsLevel(uuid, modifikations);
        write(uuid.toString(), modifikations.getColumn(), (currentLevel + 1));
    }

    public int getModifikationsLevel(UUID uuid, Modifikations modifikations) {
        if(!checkPrimary(uuid.toString())){
            return 0;
        }
        int level = (int)read(uuid.toString(), modifikations.getColumn());
        return level;
    }



    @Override
    public void defaultWrite(String primaryKey, String key, Object value) {
        String query = String.format(updateQuery, getTableName());
        try (PreparedStatement statement = getConnection().prepareStatement(query)){
            statement.setString(1, key);
            statement.setObject(2, key);
            statement.setString(3, getPrimaryKey());
            statement.setString(4, primaryKey);
            statement.executeUpdate();
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    @Override
    public Object defaultRead(String primaryKey, String key) {
        if(!checkPrimary(primaryKey)){
            return -1;
        }
        String query = String.format(resultQuery, getTableName(), getPrimaryKey(), primaryKey);
        ResultSet resultSet = getResultSet(query);
        try {
            return resultSet.getObject(key);
        }catch(SQLException exception){
            exception.printStackTrace();
        }
        return -1;
    }

    @Override
    public boolean defaultCheckPrimary(String primaryKey) {
        String query = String.format(resultQuery, getTableName(), getPrimaryKey(), primaryKey);
        try {
            ResultSet resultSet = getResultSet(query);
            if(resultSet.next()){
                return true;
            }
        }catch(Exception exception){
            exception.printStackTrace();
            return false;
        }
        return false;
    }

    public ResultSet getResultSet(String query){
        Preconditions.checkNotNull(query);
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void connect() throws Exception{
        Class.forName(driver);
        this.connection = DriverManager.getConnection(toURL(), getCredentials().getUser(), getCredentials().getPassword());


        createTable();
    }

    private void createTable() throws Exception {
        Joiner joiner = Joiner.on(", ");
        List<String> modifkations = new LinkedList<>();
        for(Modifikations list : Modifikations.values()){
            modifkations.add(list.getColumn().concat(" INT"));
        }
        String end = joiner.join(modifkations);

        String query = "CREATE TABLE IF NOT EXISTS " + getTableName() + "(" + getPrimaryKey() + " varchar(64), " + end +")";

        PreparedStatement statement = getConnection().prepareStatement(query);
        statement.executeUpdate();
    }





    @Override
    public void disconnect() throws Exception{
        getConnection().close();
    }

    private String toURL(){
        StringBuilder builder = new StringBuilder("jdbc:mysql://")
                .append(getCredentials().getHost())
                .append(":")
                .append(getCredentials().getPort())
                .append("/")
                .append(getCredentials().getDatabase());
        return builder.toString();
    }

    @Override
    public boolean isOpen() throws Exception {
        return this.connection != null && !this.connection.isClosed() && this.connection.isValid(10);
    }


}
